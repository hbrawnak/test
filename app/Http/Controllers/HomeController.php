<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phoneNumber' => 'required|unique:users'
        ]);

        $name = $request['name'];
        $number = $request['phoneNumber'];

        try {
            do {
                $activation_code = rand(100000, 999999);
            } while ((Session::put(['activation_code' => $activation_code])));

            $phone_number = trim($number);

            if ((substr($phone_number, 0, 2) === "88") == false) {
                $phone_number = '88' . $phone_number;
            }

            $opts = array(
                'http'=>array(
                    'method'=>"GET",
                    'header'=>"Accept-language: en\r\n" .
                        "Cookie: foo=bar\r\n"
                )
            );

            $context = stream_context_create($opts);

            $file = file_get_contents('http://app.planetgroupbd.com/api/sendsms/plain?user=buzzally285&password=monenai1123&sender=Friend&SMSText='.$activation_code.'&GSM='.$phone_number, false, $context);

            $stepOne = [
                'name' => $name,
                'number' => $phone_number
            ];

            Session::put(['stepOne' => $stepOne]);

            return redirect()->route('getLoginConfirm');


        } catch (QueryException $e) {
            session()->flash('message', $e->getMessage());
            return redirect()->back();
        }
    }

    public function loginConfirm()
    {
        return view('auth.loginConfirm');
    }

    public function verify(Request $request)
    {
        $this->validate($request, [
            'code' => 'required'
        ]);

        $code = $request['code'];

        $sessionCode = session('activation_code');

        if($code == $sessionCode)
        {

            $userData = session('stepOne');

            $name = $userData['name'];
            $phoneNumber = $userData['number'];

            $pastUser = User::where('phoneNumber', $phoneNumber)->first();

            if ($pastUser)
            {
                //echo $pastUser->name;
               Auth::login($pastUser);
               return redirect()->route('chat', ['name' => $pastUser->name]);
            }
            else {
                $user = new User();
                $user->name = $name;
                $user->phoneNumber = $phoneNumber;

                $user->save();
                Auth::login($user);

                return redirect()->route('chat', ['name' => $user->name]);
            }
        }
        else{
            return redirect()->back()->with(['error' => 'OTP does not match']);
        }
    }

    public function chat()
    {
        return view('chat');
    }

    public function endChat()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function userForm()
    {
        return view('data');
    }
}
