var $inp = $('#rating-input');
if ($inp.length) {

$inp.rating({
  min: 0,
  max: 5,
  step: 1,
  size: 'xs',
  showClear: false
});
}

paceOptions = {
  elements: false,
  ajax: false, // disabled
  document: false, // disabled
  eventLag: false, // disabled
  
};
$(window).load(function() {
$('.preload-block').fadeOut();
}); 
// Affix
$('.navbar-default').affix({
    offset: {
        top: 200,
        bottom: function () {
            return (this.bottom = $('.footer').outerHeight(true))
        }
    }
});

//Search
$('.search-modal').magnificPopup({
    type: 'inline',
    preloader: true,
    focus: '#search-input',
    mainClass: 'mfp-with-zoom',
    modal: true
});
$(document).on('click', '.search-modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});

// Slider
$('.home-slider, .design-carousel, .civil-works').owlCarousel({
    items: 1,
    margin: 0,
    loop: true, 
    autoplay: true,
    dots: true
});

// Slider
$('.awards-carousel').owlCarousel({
    items: 2,
    margin: 30,
    loop: true, 
    autoplay: true,
    dots: true
});

if ( $('.isotope-gallery').length ){
			
    $(".isotope-gallery").isotope({
        itemSelector: ".gallery-item",
        layoutMode: 'masonry',
        masonry: {
            columnWidth: '.grid-sizer'
        }
    });

    // Add isotope click function
    $(".gallery-filter li").on('click',function(){
        $(".gallery-filter li").removeClass("active");
        $(this).addClass("active");

        var selector = $(this).attr("data-filter");
        $(".filterable-gallery").isotope({
            filter: selector
        })
    })                
}

if ( $('.print-page').length ){			
    $(".print-page").on('click', function(){
		window.print();
		return false
	})
}

if ( $('#mapBox').length ){
	var $lat = $('#mapBox').data('lat');
	var $lon = $('#mapBox').data('lon');
	var $zoom = $('#mapBox').data('zoom');
	var $marker = $('#mapBox').data('marker');
	var $info = $('#mapBox').data('info');
	var map = new GMaps({
		el: '#mapBox',
		lat: $lat,
		lng: $lon,
		scrollwheel: false,
		scaleControl: true,
		streetViewControl: false,
		panControl: true,
		disableDoubleClickZoom: true,
		mapTypeControl: false,
		zoom: $zoom,
		styles: [
			{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},          
			{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},
			{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},
			{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},
			{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},
			{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},
			{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},
			{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},
			{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},
			{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},
			{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}
		]
	});

	map.addMarker({
		lat: $lat,
		lng: $lon,
		icon: $marker,				
		infoWindow: {
			content: $info
		}
	})
}

// document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')







// (function () {
//     var Message;
//     Message = function (arg) {
//         this.text = arg.text, this.message_side = arg.message_side;
//         this.draw = function (_this) {
//             return function () {
//                 var $message;
//                 $message = $($('.message_template').clone().html());
//                 $message.addClass(_this.message_side).find('.text').html(_this.text);
//                 $('.messages').append($message);
//                 return setTimeout(function () {
//                     return $message.addClass('appeared');
//                 }, 0);
//             };
//         }(this);
//         return this;
//     };
//     $(function () {
//         var getMessageText, message_side, sendMessage;
//         message_side = 'right';
//         getMessageText = function () {
//             var $message_input;
//             $message_input = $('.message_input');
//             return $message_input.val();
//         };
//         sendMessage = function (text) {
//             var $messages, message;
//             if (text.trim() === '') {
//                 return;
//             }
//             $('.message_input').val('');
//             $messages = $('.messages');
//             message_side = message_side === 'left' ? 'right' : 'left';
//             message = new Message({
//                 text: text,
//                 message_side: message_side
//             });
//             message.draw();
//             return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
//         };
//         $('.send_message').click(function (e) {
//             return sendMessage(getMessageText());
//         });
//         $('.message_input').keyup(function (e) {
//             if (e.which === 13) {
//                 return sendMessage(getMessageText());
//             }
//         });
//         sendMessage('Hello Philip! :)');
//         setTimeout(function () {
//             return sendMessage('Hi Sandy! How are you?');
//         }, 1000);
//         return setTimeout(function () {
//             return sendMessage('I\'m fine, thank you!');
//         }, 2000);
//     });
// }.call(this));