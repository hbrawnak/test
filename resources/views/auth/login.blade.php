<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login</title>

    <!-- Bootstrap -->
    <script src="{{ asset('vendors/pace/pace.min.js') }}"></script>
    <link href="{{ asset('vendors/pace/pace-theme-center-simple.tmpl.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="background-image: url({{ asset('images/maks_group_1.png') }}); background-size: cover; background-repeat: no-repeat; background-position: center;">
<div class="preload-block vph-100 text-center" style="background-image: url({{ asset('images/preload.png') }});">
    <div class="Aligner">
        <div class="Aligner-item Aligner-item--fixed">
            <img src="{{ asset('images/preload-logo.png') }}" alt="">
        </div>
    </div>
</div>
<section class="login_page text-center" >
<div class="container">
    <div class="row">
        <div class="text-center">
            <img src="{{ asset('images/logo.png') }}" alt="">
            <p>Help your users connect with each other like never before.</p>


            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="login_page_form" method="post" action="{{ route('postLogin') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Your Name</label>
                    <input type="text" name="name" class="form-control text-center" id="name" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="phone_num">Phone Number</label>
                    <input type="text" name="phoneNumber" class="form-control text-center" id="phone_num" placeholder="01XXXXXXXXXX">
                </div>
                <button type="submit" class="btn btn-default">Next</button>
            </form>
        </div>
    </div>
</div>
</section>

@include('partials._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
        src="http://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/magnific/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('vendors/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('vendors/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('vendors/isotope/isotope.min.js') }}"></script>
<script src="{{ asset('js/theme.js') }}"></script>
</body>
</html>