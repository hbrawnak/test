<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Chat Board</title>

    <!-- Bootstrap -->
    <script src="{{ asset('vendors/pace/pace.min.js') }}"></script>
    <link href="{{ asset('vendors/pace/pace-theme-center-simple.tmpl.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="background-image: url({{ asset('images/chat-board.png') }});background-size: cover;background-repeat: no-repeat;background-position: left bottom;">
<div class="preload-block vph-100 text-center" style="background-image: url({{ asset('images/preload.png') }});">
    <div class="Aligner">
        <div class="Aligner-item Aligner-item--fixed">
            <img src="{{ asset('images/preload-logo.png') }}" alt="">
        </div>
    </div>
</div>


<section class="chat-board adminpanel text-center" >
    <div class="container">

        <a href="#"><img class="side-logo" src="{{ asset('images/logo.png') }}" alt=""></a>
    </div>
    @yield('content')

</section>

@include('partials._footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
        src="http://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/magnific/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('vendors/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('vendors/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('vendors/isotope/isotope.min.js') }}"></script>
<script src="{{ asset('js/theme.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>