@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row" id="app">
        <div class="col-md-3 col-sm-5 pr0">
            <div class="customer-list">
                <div class="next-customer">
                    <p><img src="{{ asset('images/service-ava.png') }}" alt=""> Next Customer</p>
                </div>
                <div class="customers">
                    <ol>
                        <li><a href="#">Noman</a></li>
                        <li><a href="#">Habib</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-7 pl0">
            <div class="chat-window">
                <div class="header clearfix">
                    <img src="{{ asset('images/service-ava.png') }}" alt="">
                    <h2>Hello Support Desk</h2>
                    <a href="{{ route('end') }}" class="text-uppercase">END CONVERSATION <i class="mdi mdi-logout-variant"></i></a>
                </div>
                <div class="chat-log">
                    <ul class="chat-list">
                        <li class="chat-left">
                            <img src="#" alt="">
                            <p>Hey Let's catch up soon. Do you want to grab lunch something this weekend ?</p>
                            <div class="clearfix">
                                <small>Hello Support, 4:09 PM</small>
                            </div>
                        </li>
                        <message

                                v-for="value in chat.message"

                        >
                            @{{ value }}
                        </message>
                    </ul>
                </div>
                <div class="chat-footer">
                    {{--<div class="typing">--}}
                        {{--<p><i class="mdi mdi-dots-horizontal"></i> Hello Support is typing</p>--}}
                    {{--</div>--}}
                    <div class="input-sections">
                          <input type="text" class="form-control" placeholder="Type your message..." v-model="message" @keyup.enter="send">

                        <div class="attachments">
                            <a href="" class="mdi mdi-emoticon"></a>
                            <a href="" class="mdi mdi-image-area"></a>
                            <a href="" class="mdi mdi-attachment"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection