<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', [
    'uses' => 'HomeController@login',
    'as' => 'login'
]);
Route::post('/login', [
    'uses' => 'HomeController@postLogin',
    'as' => 'postLogin'
]);
Route::get('/loginConfirm', [
    'uses' => 'HomeController@loginConfirm',
    'as' => 'getLoginConfirm'
]);
Route::post('/loginConfirm', [
    'uses' => 'HomeController@verify',
    'as' => 'verify'
]);
Route::get('/@{name}', [
    'uses' => 'HomeController@chat',
    'as' => 'chat'
])->middleware('auth');
Route::get('/endchat', [
    'uses' => 'HomeController@endChat',
    'as' => 'end'
]);
Route::get('/user', [
    'uses' => 'HomeController@userForm',
    'as' => 'user'
]);
Route::post('/{name}', [
    'uses' => 'HomeController@userData',
    'as' => 'postUser'
]);

//Route::get('/{name}', [
//    'uses' => 'HomeController@redirectTo',
//    'as' => 'redirectTo'
//]);
